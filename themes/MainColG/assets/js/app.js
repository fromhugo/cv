console.log("==> app.js <==")

const gtCols = document.getElementById('gt-cols');
if (gtCols) {
    // console.log({gtCols}, gtCols.children.length)
    if (gtCols.children.length === 1) {
        // console.log('on en a 1')
        gtCols.classList.add('children-1')
    } else if (gtCols.children.length === 2) {
        // console.log('on en a 2')
        gtCols.classList.add('children-2')
    } else if (gtCols.children.length === 3) {
        // console.log('on en a 3')
        gtCols.classList.add('children-3')
    } else if (gtCols.children.length >= 4) {
        // console.log('on en a 4 ou plus')
        gtCols.classList.add('children-4')
    }
}
// Form validation
function toggleHelp(elt) {
    elt.parentElement.nextElementSibling.classList.toggle("is-invisible")
}

function hideHelp(elt) {
    if (!elt.parentElement.nextElementSibling.classList.contains("is-invisible")) {
        elt.parentElement.nextElementSibling.classList.add("is-invisible")
    }
}

function addIsSuccess(elt) {
    if (!elt.classList.contains("is-success")) {
        elt.classList.add("is-success")
    }
}

function removeIsSuccess(elt) {
    if (elt.classList.contains("is-success")) {
        elt.classList.remove("is-success")
    }
}

function addIsDanger(elt) {
    if (!elt.classList.contains("is-danger")) {
        elt.classList.add("is-danger")
    }
}

function removeIsDanger(elt) {
    if (elt.classList.contains("is-danger")) {
        elt.classList.remove("is-danger")
    }
}

function goodEntry(elt) {
    addIsSuccess(elt)
    removeIsDanger(elt)
}

function badEntry(elt) {
    removeIsSuccess(elt)
    addIsDanger(elt)
}

function manageSubmit(a, b, c, d, e, f, g, btn) {
    if (btn.hasAttribute('disabled') && (a && b && c && d && e && f && g)) {
        btn.removeAttribute('disabled')
    } else if (!btn.hasAttribute('disabled') && !(a && b && c && d && e && f && g)) {
        btn.setAttribute('disabled', ' ')
    }
}

function disableSubmit(btn) {
    if (!btn.hasAttribute('disabled')) {
        btn.setAttribute('disabled', " ")
    }
}

const form = document.getElementsByTagName('form');

if (form.length > 0) {
    if (document.getElementById('contact-form').length > 0) {
        const submitButton = document.getElementById('submit-button');
        const resetButton = document.getElementById('reset-button');
        const lastName = document.getElementById("last-name");
        const firstName = document.getElementById("first-name");
        const eMail = document.getElementById("email");
        const telePhone = document.getElementById("phone");
        const subJect = document.getElementById("subject");
        const meSsage = document.getElementById("message");
        const rgPd = document.getElementById("rgpd");
        let lastNameValidity = false;
        let firstNameValidity = false;
        let eMailValidity = false;
        let telePhoneValidity = false;
        let subJectValidity = false;
        let meSsageValidity = false;
        let rgPdValidity = false;

        function testValidity(elt) {
            console.log(elt, {lastNameValidity}, {firstNameValidity}, {eMailValidity}, {telePhoneValidity}, {subJectValidity}, {meSsageValidity}, {rgPdValidity})
        }

        lastName.addEventListener('input', function(){
            // console.log(validator.isEmpty(lastName.value))
            if (validator.isEmpty(lastName.value) === false ) {
                if (validator.isAlpha(lastName.value, 'fr-FR',{ignore: " -  ' , . ? ! : _ "})) {
                    lastNameValidity = true;
                    // testValidity('lastName plein et valide');
                    manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                    goodEntry(lastName)
                    hideHelp(lastName)
                } else {
                    // testValidity('lastName plein et non valide');
                    if (lastNameValidity === true) {
                        lastNameValidity = false;
                    }
                    badEntry(lastName)
                    toggleHelp(lastName)
                }
            } else {
                if (lastNameValidity === true) {
                    lastNameValidity = false;
                }
                // testValidity('lastName vide avant validation :')
                manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                badEntry(lastName)
                toggleHelp(lastName)
            }
        })
        firstName.addEventListener('input', function(){
            // console.log(validator.isEmpty(firstName.value))
            if (validator.isEmpty(firstName.value) === false ) {
                if (validator.isAlpha(firstName.value, 'fr-FR',{ignore: " -  ' , . ? ! : _ "})) {
                    firstNameValidity = true;
                    // testValidity('firstName plein et valide')
                    manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                    goodEntry(firstName)
                    hideHelp(firstName)
                } else {
                    // testValidity('firstName plein et non valide');
                    if (firstNameValidity === true) {
                        firstNameValidity = false;
                    }
                    badEntry(firstName)
                    toggleHelp(firstName)
                }
            } else {
                if (firstNameValidity === true) {
                    firstNameValidity = false;
                }
                badEntry(firstName)
                // testValidity('firstName vide avant validation :')
                manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                toggleHelp(firstName)
            }
        })
        eMail.addEventListener('input', function(){
            // console.log(validator.isEmpty(eMail.value))
            if (!validator.isEmpty(eMail.value)) {
                if (validator.isEmail(eMail.value)) {
                    eMailValidity = true;
                    eMail.value = validator.normalizeEmail(eMail.value,{
                        all_lowercase: true,
                        gmail_lowercase: true,
                        gmail_remove_dots: true,
                        gmail_remove_subaddress: true,
                        gmail_convert_googlemaildotcom: true,
                        outlookdotcom_lowercase: true,
                        outlookdotcom_remove_subaddress: true,
                        yahoo_lowercase: true,
                        yahoo_remove_subaddress: true,
                        icloud_remove_subaddress: true
                    })
                    // testValidity('eMail plein et valide')
                    manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                    goodEntry(eMail)
                    hideHelp(eMail)
                } else {
                    // testValidity('eMail plein et non valide');
                    if (eMailValidity === true) {
                        eMailValidity = false;
                    }
                    badEntry(eMail)
                    toggleHelp(eMail)
                }
            } else {
                if (eMailValidity === true) {
                    eMailValidity = false;
                }
                badEntry(eMail)
                // testValidity('eMail vide avant validation :')
                manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                toggleHelp(eMail)
            }
        })
        telePhone.addEventListener('input', function(){
            // console.log(validator.isEmpty(telePhone.value))
            if (!validator.isEmpty(telePhone.value)) {
                let cleanTelePhone = validator.blacklist(telePhone.value, '\\.\\-\\_ ');
                telePhone.value = cleanTelePhone;
                if (validator.isMobilePhone(cleanTelePhone)) {
                    telePhoneValidity = true;
                    // testValidity('telePhone plein et valide')
                    manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                    goodEntry(telePhone)
                    hideHelp(telePhone)
                } else {
                    // testValidity('eMail plein et non valide');
                    if (telePhoneValidity === true) {
                        telePhoneValidity = false;
                    }
                    badEntry(telePhone)
                    toggleHelp(telePhone)
                }
            } else {
                if (telePhoneValidity === true) {
                    telePhoneValidity = false;
                }
                badEntry(telePhone)
                // testValidity('telePhone vide avant validation :')
                manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                toggleHelp(telePhone)
            }
        })
        subJect.addEventListener('input', function(){
            // console.log(validator.isEmpty(subJect.value))
            if (!validator.isEmpty(subJect.value)) {
                let cleanedSubJect = validator.blacklist(subJect.value, '\\[\\]<>');
                subJect.value = cleanedSubJect;
                subJectValidity = true;
                // testValidity('subJect plein et valide')
                manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                goodEntry(subJect)
                hideHelp(subJect)
            } else {
                if (subJectValidity === true) {
                    subJectValidity = false;
                }
                badEntry(subJect)
                // testValidity('subJect vide avant validation :')
                manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                toggleHelp(subJect)
            }
        })
        meSsage.addEventListener('input', function(){
            // console.log(validator.isEmpty(meSsage.value))
            if (!validator.isEmpty(meSsage.value)) {
                let cleanedMeSsage = validator.blacklist(meSsage.value, '\\[\\]<>');
                meSsage.value = cleanedMeSsage;
                meSsageValidity = true;
                // testValidity('meSsage plein et valide')
                manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                goodEntry(meSsage)
                hideHelp(meSsage)
            } else {
                if (meSsageValidity === true) {
                    meSsageValidity = false;
                }
                badEntry(meSsage)
                // testValidity('meSsage vide avant validation :')
                manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
                toggleHelp(meSsage)
            }
        })
        rgPd.addEventListener('click', function(){
            // console.log(rgPd.checked)
            rgPdValidity = rgPd.checked;
            // testValidity('rgPd au click :')
            manageSubmit(lastNameValidity, firstNameValidity, eMailValidity, telePhoneValidity, subJectValidity, meSsageValidity, rgPdValidity, submitButton)
            if (rgPdValidity) {
                goodEntry(rgPd)
            } else {
                badEntry(rgPd)
            }
        })
        resetButton.addEventListener("click", function(e) {
            e.preventDefault();
            lastName.value = "";
            firstName.value = "";
            eMail.value = "";
            telePhone.value = "";
            subJect.value = "";
            meSsage.value = "";
            rgPd.checked = false;
            disableSubmit(submitButton);
        })
    } // end contact-form
    
} else {
    console.log('pas de form')
}




