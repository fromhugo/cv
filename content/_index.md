---
title: Jane Doe
date: 2020-11-28T17:35:06+01:00
draft: false
description: "Lorem ipsum dolor sit amet. Praesentium magnam consectetur vel in deserunt aspernatur est reprehenderit sunt hic"
image: images/girl.jpg
job: Webdesigner
location: Marseille
email: ex@mail.com
tel: "O6 66 09 61 09"
fonts: ""
colors:
  mainBgcColor: ""
  highlightColor: ""
  subtitleColor: ""
  linkColor: ""
  linkHoverColor: ""
---